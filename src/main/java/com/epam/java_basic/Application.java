package com.epam.java_basic;
import  java.util.Scanner;
/**
 * Application's entry point, use it to demonstrate your code execution
 */
public class Application {

    public static void main(String[] args) {
        //throw new UnsupportedOperationException("You need to implement this method");
        String ch="Y";
        do{
        String s1 = getInput("Enter the first number: ");
        String s2 = getInput("Enter the second number: ");
        String operator = getInput("Enter operator (+, -, *, /): ");

        double result = 0;



            switch (operator) {
                case "+":
                    result = add(s1, s2);
                    break;
                case "-":
                    result = subtract(s1, s2);
                    break;
                case "*":
                    result = multiply(s1, s2);
                    break;
                case "/":
                    result = div(s1, s2);
                    break;
                default:
                    System.out.println("Bye!");
                    break;
            }
            ch = getInput("Do you want to continue? (Y/N)");

        }while(ch=="Y"|| ch=="y");

    }
    static String getInput(String prompt) {//METHOD FOR GETTING THE INPUT FROM THE USER: getInput (WITH String ARGUMENT prompt)
        System.out.print(prompt);
        Scanner console = new Scanner(System.in);
        return console.nextLine();
    }

    static double add(String st1, String st2) {//METHOD FOR ADDITION: addition (WITH 2 String ARGUMENTS st1 AND st2)
        double d1 = Double.parseDouble(st1);
        double d2 = Double.parseDouble(st2);
        return d1 + d2;
    }

    static double subtract(String st1, String st2) {//METHOD FOR SUBTRACTION: subtraction (WITH 2 String ARGUMENTS st1 AND st2)
        double d1 = Double.parseDouble(st1);
        double d2 = Double.parseDouble(st2);
        return d1 - d2;
    }

    static double multiply(String st1, String st2) {//METHOD FOR MULTYPLYNG: multyplyng (WITH 2 String ARGUMENTS st1 AND st2)
        double d1 = Double.parseDouble(st1);
        double d2 = Double.parseDouble(st2);
        return d1 * d2;
    }

    static double div(String st1, String st2) {//METHOD FOR DIVISION: division (WITH 2 String ARGUMENTS st1 AND st2)
        double d1 = Double.parseDouble(st1);
        double d2 = Double.parseDouble(st2);
        return d1 / d2;
    }

}
